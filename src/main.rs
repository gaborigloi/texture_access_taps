//! Reducing the number of texture accesses for neighbouring taps.
//!
//! When we want to sum a rectangular region with weights like:
//!
//! ```
//! d a
//! c b
//! ```
//!
//! We can compute the sum using one texture access by exploiting
//! the linear interpolation available,
//! provided the following condition holds:
//!
//! ```ac = bd```
//!
//! and a, b, c, d have the same sign.
//!
//! # Limitations
//! **WARNING:** The accuracy of the interpolation done by the shader is limited.
//! The accuracy of the texture coordinates is limited to 8 fractional bits (1/256). See
//!
//! * <http://gamedev.stackexchange.com/questions/101953/low-quality-bilinear-sampling-in-webgl-opengl-directx/102397#102397>
//! * <http://ww.w.gpucomputing.net/sites/default/files/papers/664/10318.pdf#page=5>
//! * There are 256-1=255 intermediate values between two pixels of a texture.
//!
//! See oldschool_gpgpu_centred_offset_linear_vs_nearest_interpolation.rs for a demonstration.
//! This method should only be used when we do not require good accuracy,
//! when an approximate sum is suffficient.
//!
//! ## Interpolation along one edge
//! When both weights on the same edge are zero, the above equation will hold: `ac = 0 = bc = 0`.
//! We obtain linear interpolation along the edge between the two non-zero points as a special case.
//!
//! ##### Gaussian blur
//! This will always hold for any 2×2 square in a Gaussian blur filter:
//!
//! Due to the exponential, the multiplication becomes simple addition:
//! <https://en.wikipedia.org/wiki/Gaussian_blur#Mathematics>.
//!
//! ac = e<sup>−((x+d)^2 + (y+d)^2 )</sup> e<sup>−( x^2 + y^2 )</sup>
//! = bd = e<sup>−( (x+d)^2 + y^2 )</sup> e<sup>−(x^2 + (y+d)^2 )</sup>
//! = e<sup>−( x^2 + y^2 + (x+d)^2 + (y+d)^2 )</sup>
//!
//! (We ignored the constant factors here, they do not make a difference in this case).
//!
//! # References:
//! * <http://rastergrid.com/blog/2010/09/efficient-gaussian-blur-with-linear-sampling/>.
//! * <http://rastergrid.com/blog/2010/09/efficient-gaussian-blur-with-linear-sampling/#comment-65003>
//!     * The first recommendation, (9-tap Gaussian filter using only 2 texture fetches in 2 passes)
//!       is two convolution passes (blurring in two steps) with the kernel
//!
//!       ```
//!       [1 1]
//!       [1 1]
//!       ```
//!
//!       If we convolve this filter `f` with itself, we obtain the standard 9-tap Gaussian shown in the comment.
//!
//!       This works because convolution is [associative](https://en.wikipedia.org/wiki/Convolution#Algebraic_properties):
//!       we can blur the image in two passes `(I*f)*f`, which is equivalent
//!       to blurring in one pass with the larger 9-tap filter: `I*(f*f)`.

#[macro_use] extern crate text_io;

fn main() {
    println!(r#"
d a
c b"#);
    println!("a:");
    let a: f32 = read!();
    println!("b:");
    let b: f32 = read!();
    println!("c:");
    let c: f32 = read!();
    println!("d:");
    let d: f32 = read!();

    println!("Checking if a * c == b * d: {} == {}", a * c, b * d);
    if a * c == b * d {
        println!("a * c == b * d");
        println!("We can use one texel fetch to sum the four texels with the given coefficients.");
    } else {
        println!("a * c != b * d");
        println!("We need (at least) two texel fetches to sum the four texels with the given coefficients.");
    }

    let z = a + b + c + d;
    println!("z = a+b+c+d = {}", z);
    let y = (b + c) / (a + b + c + d);
    println!("y = (b+c)/(a+b+c+d) = {}", y);
    let x = (d + c) / (a + b + c + d);
    println!("x = (d+c)/(a+b+c+d) = {}", x);

    let print_interpolated_weights = |x: f32, y: f32, z: f32| {
        let a_i = z * (1.0 - x) * (1.0 - y);
        println!("a = z(1-x)(1-y) = {}", a_i);
        print_errors(a, a_i);

        let b_i = z * (1.0 - x) * y;
        println!("b = z(1-x)y = {}", b_i);
        print_errors(b, b_i);

        let c_i = z * x * y;
        println!("c = zxy = {}", c_i);
        print_errors(c, c_i);

        let d_i = z * x * (1.0 - y);
        println!("d = zx(1-y) = {}", d_i);
        print_errors(d, d_i);
    };

    println!("\nvalues reconstructed from f32 interpolation:");
    print_interpolated_weights(x, y, z);

    println!("\ntruncate to texture coordinate precision:");
    let x_tex = truncate_to_texture_coordinate("x", x);
    let y_tex = truncate_to_texture_coordinate("y", y);
    let z_tex = truncate_to_texture_coordinate("z", z);
    println!("\nvalues reconstructed using texture coordinate accuracy:");
    print_interpolated_weights(x_tex, y_tex, z_tex);
}

fn truncate_to_texture_coordinate(name: &str, coord: f32) -> f32 {
    let tex_coord = (coord * 256.0).round() / 256.0;
    println!("{} truncated to {}", name, tex_coord);
    print_errors(coord, tex_coord);
    tex_coord
}

fn print_errors(value: f32, approximation: f32) {
    println!("\tabsolute error: {}", (value - approximation).abs());
    println!("\trelative error: {}", (value - approximation).abs() / value.abs());
}
